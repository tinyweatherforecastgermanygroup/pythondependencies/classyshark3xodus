# ExodusPrivacy NEW database (eof419),
# hotFix permissionsExplorer(Chairlock): sigma-stats 

552 exoTrackers(432) = 518+34 signatures for 401+31 identified trackers

More info : 
- https://reports.exodus-privacy.eu.org/api/trackers
- https://gitlab.com/oF2pks/3xodusprivacy-toolbox (31 known additions ²/ETIP-standby °/missing µ/tools)
- https://etip.exodus-privacy.eu.org


*Exception to Exodus database: Mapbox is softened to MapboxTelemetry instead of the wider general map sdk detection https://docs.mapbox.com/android/maps/overview/#telemetry-opt-out
