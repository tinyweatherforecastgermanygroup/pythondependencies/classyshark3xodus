package com.oF2pks.chairlock;

import android.content.pm.ApplicationInfo;
import android.content.pm.PermissionInfo;

import com.oF2pks.applicationsinfo.utils.Tuple;


public class PermissionItem {
    PermissionInfo permInfo;
    //PackageInfo packPermInfo;

    ApplicationInfo applicationInfo;
    String label;
    boolean star;
    String group="";
    String packageList="";
    String description="";
    Long date;
    Long size = -0L;
    Tuple sha;
}
