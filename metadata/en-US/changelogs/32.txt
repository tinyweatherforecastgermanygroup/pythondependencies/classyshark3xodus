# ...ExodusPrivacy NEW database (eof443),

598 exoTrackers(453) = 567+31 signatures for 424+29 identified trackers

# upSDK:30 + permission.QUERY_ALL_PACKAGES

# Chairlock/AI on par:
  * inactive apps: permission.PACKAGE_USAGE_STATS for android 10 (Q) & up
  * viewManifest with search
  * permissions: [internal]protectionLevel + flags/ui...., + Base64 sig

More info : 
- https://reports.exodus-privacy.eu.org/api/trackers
- https://gitlab.com/oF2pks/3xodusprivacy-toolbox (31 known additions ²/ETIP-standby °/missing µ/tools)
- https://etip.exodus-privacy.eu.org

