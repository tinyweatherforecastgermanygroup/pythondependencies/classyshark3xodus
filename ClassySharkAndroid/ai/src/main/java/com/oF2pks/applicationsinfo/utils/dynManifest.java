/**
 * prim -origin : https://github.com/iSECPartners/manifest-explorer/blob/873468b2ea837490779ba16ac45de52ccfd0fd9d/src/com/isecpartners/android/manifestexplorer/ManifestExplorer.java
 * commit 873468b on Aug 21, 2012
 * (+GPL-2.0 added on Nov 30, 2012)
 *
 * Tool for exploring the configuration of proprietary Android builds. Also
 * useful for validating the configuration of non-proprietary builds. Originally
 * wrote this for analyzing the pre-open source versions of Android that were
 * shipping only in the SDK. These versions didn't expose the critical
 * AndroidManifest.xml system file, but this tool lets you see that information
 * as the system stores it for use at runtime and makes it generally available
 * to processes. Other proprietary builds of Android may also not provide this
 * system file, which defines much of the security policy of the platform, and
 * is needed for system hardening and to understand what protections of user
 * data are in place.
 *
 * @author Jesse (c) 2008-2009, iSEC Partners
 *
 */

package com.oF2pks.applicationsinfo.utils;

import static com.oF2pks.applicationsinfo.utils.Utils.Html5Escapers;

import android.content.res.Resources;
import android.content.res.XmlResourceParser;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

public class dynManifest {

    protected static void insertSpaces(StringBuffer sb, int num) {
        if (sb == null)
            return;
        for (int i = 0; i < num; i++)
            sb.append(" ");
    }
    private static CharSequence getAttribs(XmlResourceParser xrp, Resources currentResources) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < xrp.getAttributeCount(); i++){
            if (xrp.getAttributeName(i).length()!=0)
                sb.append("\n" + xrp.getAttributeName(i) + "=\""
                        + resolveValue(xrp.getAttributeValue(i), currentResources)
                        + "\"");
            else
                sb.append("\n" + xrp.getAttributeType(i)
                        +Integer.toHexString(xrp.getAttributeNameResource(i)) + "=\""
                        + resolveValue(xrp.getAttributeValue(i), currentResources)
                        + "\"");
        }
        return sb;
    }
    private static String resolveValue(String in, Resources r) {
        if (in == null )
            return "null";
        if (!in.startsWith("@"))
            return in;
        int num = 0;
        try {
            num = Integer.parseInt(in.substring(1));
            return Html5Escapers(r.getString(num));
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return in;
        } catch (RuntimeException e) {
            // formerly noted errors here, but simply not resolving works better
            //e.printStackTrace();//Resources.NotFoundException!! W/System.err:     at com.oF2pks.applicationsinfo.View2ManifestActivity.resolveValue(View2ManifestActivity.java:185)
            try {
                if (r.getResourceEntryName(num).length()>0)
                    return r.getResourceTypeName(num)+"/"+r.getResourceEntryName(num);
                else return r.getResourceTypeName(num)+"/"+in;
            } catch (Resources.NotFoundException e2){
                e2.printStackTrace();
                return in;
            }
        }
    }

    public static CharSequence getXMLText(XmlResourceParser xrp, Resources currentResources) {
        StringBuffer sb = new StringBuffer();
        int indent = 0;
        try {
            int eventType = xrp.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                // for sb
                switch (eventType) {
                    case XmlPullParser.START_TAG:
                        indent += 1;
                        sb.append("\n");
                        insertSpaces(sb, indent);
                        sb.append("<" + xrp.getName());
                        sb.append(getAttribs(xrp, currentResources));
                        sb.append(">");
                        break;
                    case XmlPullParser.END_TAG:
                        indent -= 1;
                        sb.append("\n");
                        insertSpaces(sb, indent);
                        sb.append("</" + xrp.getName() + ">");
                        break;

                    case XmlPullParser.TEXT:
                        sb.append("" + xrp.getText());
                        break;

                    case XmlPullParser.CDSECT:
                        sb.append("<!CDATA[" + xrp.getText() + "]]>");
                        break;

                    case XmlPullParser.PROCESSING_INSTRUCTION:
                        sb.append("<?" + xrp.getText() + "?>");
                        break;

                    case XmlPullParser.COMMENT:
                        sb.append("<!--" + xrp.getText() + "-->");
                        break;
                }
                eventType = xrp.nextToken();
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
            //showError("Reading XML", ioe);
        } catch (XmlPullParserException xppe) {
            xppe.printStackTrace();
            //showError("Parsing XML", xppe);
        }
        return sb;
    }

}
