package com.google.classysharkandroid.activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.SearchView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.classysharkandroid.R;
import com.google.classysharkandroid.utils.PackageUtils;
import com.google.classysharkandroid.utils.UriUtils;
import com.oF2pks.xmlapkparser.AXMLPrinter;

import java.io.File;
import java.lang.ref.WeakReference;

import static com.google.classysharkandroid.utils.IOUtils.deleteCache;
import static com.google.classysharkandroid.utils.PackageUtils.apkPro;
import static com.google.classysharkandroid.utils.UriUtils.stringUri;
import static com.oF2pks.applicationsinfo.utils.Utils.getProperXml;

/**
 * Activity that shows AndroidManifest.xml of any apps. Application package name must be passed as extra.
 * To correctly display returned xml file, we show it in a {@link WebView}. The way we do that
 * is not very natural, but it's the simplest way to do that.
 * So, asynchronously, we get raw xml string and save it to a file. Then we ask to WebView to display this file,
 * by this way, WebView auto detect xml and display it nicely.
 * File do not need to be kept. We delete it to keep used memory as low as possible, but anyway, each time
 * we will show application's manifest, the same file will be used, so used memory will not grow.
 */
public class ViewManifestActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    public static final String EXTRA_PACKAGE_NAME = "package_name";
    private ActionBar bar;

    private WebView mWebView;
    private ProgressDialog mProgressDialog;

    private static String code;
    private Intent intent;
    private  PackageManager mPackageManager;

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bar = getSupportActionBar();
        //bar.setDisplayHomeAsUpEnabled(true);
        bar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        bar.setCustomView(R.layout.manifest_bar);
        TextView tTitle = findViewById(R.id.title_text);
        assert tTitle != null;

        androidx.appcompat.widget.SearchView searchView = findViewById(R.id.search_butn);
        //searchView.setQueryHint("zz");
        searchView.setOnQueryTextListener(this);


        Button eScan = findViewById(R.id.scan_button);
        assert eScan != null;
        eScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PackageUtils.manifestPackage(getBaseContext().getResources(), code, ViewManifestActivity.this);
            }
        });

        mWebView = new WebView(this);
        WebSettings settings = mWebView.getSettings();
        settings.setBuiltInZoomControls(true);
        settings.setUseWideViewPort(true);
        setContentView(mWebView);
        //sdk30 mWebView.getSettings().setAllowContentAccess(true);mWebView.getSettings().setAllowFileAccess(true);


        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setCancelable(true);
        mProgressDialog.setMessage(getString(R.string.loading));

        String filePath = null, applicationLabel = null;
        intent = getIntent();
        mPackageManager= getPackageManager();

        if (intent.getAction() != null) {
            if (intent.getData().getLastPathSegment().endsWith(".apk")) {
                intent.setClass(this, ClassesListActivity.class);
                startActivity(intent);
                finishAndRemoveTask();
            }

            code = stringUri(ViewManifestActivity.this,intent.getData());
            if (Build.VERSION.SDK_INT >= 23) {
                if (!isStoragePermission()) {
                    //ActivityCompat.finishAffinity(this);
                } else {
                    Toast.makeText(this, "permission STORAGE granted", Toast.LENGTH_LONG).show();
                    showProgressBar(true);
                    //longClick = false;
                }
            } else {
                showProgressBar(true);
            }
            displayContent();
            try {
                tTitle.setText("\u2707" + intent.getData().getLastPathSegment());
            } catch (NullPointerException e){
                tTitle.setText("\u2707 Content");
            }
        } else {
            showProgressBar(true);
            PackageInfo packageInfo;
            final String packageName = intent.getStringExtra(EXTRA_PACKAGE_NAME);
            if (packageName != null) {
                try {
                    packageInfo = mPackageManager.getPackageInfo(packageName,
                            PackageManager.GET_SIGNATURES
                                    | PackageManager.GET_PERMISSIONS);
                    applicationLabel = mPackageManager.getApplicationInfo(packageName, 0).loadLabel(getPackageManager()).toString();
                    new AlertDialog.Builder(this).setTitle("\u2139\u2668: " + applicationLabel)
                            .setNegativeButton(android.R.string.ok, null)
                            .setMessage(apkPro(packageInfo, mPackageManager)).show();

                    filePath = mPackageManager.getPackageInfo(packageName, 0).applicationInfo.sourceDir;
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                    Toast.makeText(this, packageName + ": " + getString(com.oF2pks.applicationsinfo.R.string.app_not_installed), Toast.LENGTH_LONG).show();
                    finish();
                }

                tTitle.setText("\u2707" + applicationLabel);

            } else {
                tTitle.setText(intent.getData().toString());
                filePath = intent.getData().getPath();
            }
            new AsyncManifestLoader(ViewManifestActivity.this).execute(filePath);
            //displayContent();
        }
    }


    @TargetApi(Build.VERSION_CODES.M)
    public boolean isStoragePermission(){
        if (ContextCompat.checkSelfPermission(ViewManifestActivity.this,android.Manifest.permission.READ_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED){

            if (ActivityCompat.shouldShowRequestPermissionRationale(ViewManifestActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)){
                ActivityCompat.requestPermissions(ViewManifestActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
            }else ActivityCompat.requestPermissions(ViewManifestActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
            //ActivityCompat.finishAffinity(this);
            return false;
        } else {
            displayContent();
        } return true;
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this, "permission validation", Toast.LENGTH_LONG).show();
            //longClick = false;
        } else {
            deleteCache(this);
            Toast.makeText(ViewManifestActivity.this, "permission STORAGE DENIED" , Toast.LENGTH_LONG).show();
            ActivityCompat.finishAffinity(this);
        }
    }

    private void displayContent() {
        //Toast.makeText(this, mPath, Toast.LENGTH_LONG).show();
        mWebView.setWebChromeClient(new MyWebChromeClient());
        mWebView.findAllAsync("ComponentFactory");
        mWebView.loadData(code,"text/xml","UTF-8");;
        // ??? sdk31+ https://developer.android.com/reference/androidx/webkit/WebViewAssetLoader?hl=vi
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        mWebView.findNext(true);
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        mWebView.findAllAsync(newText);
        return true;
    }

    final class MyWebChromeClient extends WebChromeClient {
        @Override
        public void onProgressChanged(WebView view, int progress) {
            if (progress == 100)
                showProgressBar(false);
        }
    }

    private void showProgressBar(boolean show) {
        if (show)
            mProgressDialog.show();
        else
            mProgressDialog.dismiss();
    }

    /**
     * This AsyncTask takes manifest file path as argument
     */
    private static class AsyncManifestLoader extends AsyncTask<String, Integer, Boolean> {
        private WeakReference<ViewManifestActivity> mActivity = null;
        public AsyncManifestLoader (ViewManifestActivity pActivity) {
            link(pActivity);
        }

        public void link (ViewManifestActivity pActivity) {
            mActivity = new WeakReference<ViewManifestActivity>(pActivity);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if(mActivity.get() != null) mActivity.get().showProgressBar(true);
        }

        @Override
        protected Boolean doInBackground(String... strings) {
            String filePath = strings[0];
            code = getProperXml(AXMLPrinter.getManifestXMLFromAPK(filePath, "AndroidManifest.xml"));

            if (code == null) {
                filePath = UriUtils.pathUriCache(mActivity.get(), mActivity.get().intent.getData(), "cache.apk");
                code = getProperXml(AXMLPrinter.getManifestXMLFromAPK(filePath, "AndroidManifest.xml"));
                if (code == null) return false;
            }

            return true;
        }

        /**
         * Do not hide progressDialog here, WebView will hide it when content will be displayed
         */
        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if (mActivity.get() != null) {
                if (result)
                    mActivity.get().displayContent();
                else
                    mActivity.get().handleError();
            }
        }
    }
    private void handleError() {
        Toast.makeText(this, "R.string.error", Toast.LENGTH_LONG).show();
        finish();
    }


}
