/*
 * Copyright 2015 Google, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.classysharkandroid.activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.classysharkandroid.R;
import com.google.classysharkandroid.adapters.StableArrayAdapter;
import com.google.classysharkandroid.dex.DexLoaderBuilder;
import com.google.classysharkandroid.reflector.ClassesNamesList;
import com.google.classysharkandroid.reflector.Reflector;
import com.google.classysharkandroid.utils.IOUtils;
import com.google.classysharkandroid.utils.UriUtils;

import java.io.File;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Enumeration;

import dalvik.system.DexClassLoader;
import dalvik.system.DexFile;

import static com.google.classysharkandroid.utils.IOUtils.clearApplicationData;
import static com.google.classysharkandroid.utils.IOUtils.deleteCache;
import static com.google.classysharkandroid.utils.PackageUtils.apkCert;
import static com.google.classysharkandroid.utils.PackageUtils.apkIsolatedZygote;
import static com.google.classysharkandroid.utils.PackageUtils.convertS;

public class ClassesListActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    public static final String SELECTED_CLASS_NAME = "SELECTED_CLASS_NAME";
    public static final String SELECTED_CLASS_DUMP = "SELECTED_CLASS_DUMP";
    public static final String EXTRA_PACKAGE_NAME = "package_name";

    private Intent inIntent;
    private ClassesNamesList classesList;
    private ClassesNamesList classesListAll;
    private ListView lv;
    private ProgressDialog mProgressDialog;
    private int Signz = 0 ;
    private int Totalz=0;
    private int ClassTotalz=0;
    private  String msg = "THIS IS LongPress:\n\n==>COMPLETE CLASS LIST";
    private String[] Sign;
    private int[] SignStat;
    private boolean[] SignB;
    private String[] Names;
    private boolean longClick;
    private int totalTT=0;
    private StableArrayAdapter adapter;
    private String title = "Content";
    private ActionBar bar;
    private String sha256="";


    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        deleteCache(this);
        clearApplicationData(this);
        super.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bar = getSupportActionBar();
        inIntent = getIntent();

        if (inIntent.getStringExtra(MainActivity.CLICK_PRESS)==null)longClick=false;
        else if (inIntent.getStringExtra(MainActivity.CLICK_PRESS).contains("longClick"))longClick=true;
        if (Build.VERSION.SDK_INT >= 23 && inIntent.getAction()!= null) {

            if (!isStoragePermission()){
                ///ActivityCompat.finishAffinity(this);
            } else {
                Toast.makeText(ClassesListActivity.this, "permission STORAGE granted" , Toast.LENGTH_LONG).show();
                longClick = false;
            }
        } else doJob();
    }

    @TargetApi(Build.VERSION_CODES.M)
    private boolean isStoragePermission(){
        if (ContextCompat.checkSelfPermission(ClassesListActivity.this,android.Manifest.permission.READ_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED){

            if (ActivityCompat.shouldShowRequestPermissionRationale(ClassesListActivity.this,Manifest.permission.READ_EXTERNAL_STORAGE)){
                ActivityCompat.requestPermissions(ClassesListActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
            }else ActivityCompat.requestPermissions(ClassesListActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
            ///ActivityCompat.finishAffinity(this);
            return false;
        } else doJob();return true;
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(ClassesListActivity.this, "permission validation", Toast.LENGTH_LONG).show();
                    longClick = false;
                    //resume tasks needing this permission
                    doJob();
                } else {
                    deleteCache(this);
                    Toast.makeText(ClassesListActivity.this, "permission STORAGE DENIED" , Toast.LENGTH_LONG).show();
                    ActivityCompat.finishAffinity(this);
        }
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        try {
            adapter.getFilter().filter(newText);
        } catch (NullPointerException e){
            //return false;
        }
        return true;
    }

    private Runnable changeText = new Runnable() {
        @Override
        public void run() {
            mProgressDialog.setMessage(sha256);
        }
    };

    private void doJob(){
        setContentView(R.layout.activity_classes_list);

        lv = (ListView) findViewById(R.id.listView);
        lv.setTextFilterEnabled(true);

        final Uri uriFromIntent = inIntent.getData();
        classesList = new ClassesNamesList();
        classesListAll  = new ClassesNamesList();


        if(inIntent.getStringExtra(MainActivity.APP_NAME) != null)
            title = "\u2622" + inIntent.getStringExtra(MainActivity.APP_NAME);
        else title = "\u2707" + uriFromIntent.getLastPathSegment();
        bar.setTitle((Html.fromHtml("<font color=\"#FFFF80\">" +
                title + "</font>")));
        if (longClick) bar.setIcon(R.mipmap.ic_launcher);
        else bar.setIcon(R.mipmap.ic_launcher_round);
        bar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_TITLE | ActionBar.DISPLAY_SHOW_CUSTOM);


        SearchView searchView = new SearchView(bar.getThemedContext());
        searchView.setOnQueryTextListener(this);

        ActionBar.LayoutParams layoutParams = new ActionBar.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        bar.setCustomView(searchView, layoutParams);

        sha256=inIntent.getData().toString()+"\n";
        mProgressDialog = new ProgressDialog(ClassesListActivity.this);
        mProgressDialog.setIcon(R.mipmap.ic_launcher);
        mProgressDialog.setTitle("¸.·´¯`·.´¯`·.¸¸.·´¯`·.¸><(((º>");
        mProgressDialog.setMessage(sha256);
        mProgressDialog.setIndeterminate(false);
        //mProgressDialog.setCancelable(false);
        mProgressDialog.show();


        Names = getResources().getStringArray(R.array.tname);

        InputStream uriStream;
        try {


            uriStream = UriUtils.getStreamFromUri(ClassesListActivity.this,
                    uriFromIntent);

            final byte[] bytes = IOUtils.toByteArray(uriStream);
            new Thread(new Runnable() {
                public void run() {
                    try{
                        sha256+="\nMD5sum: "+convertS(MessageDigest.getInstance("md5").digest(bytes))
                                        +"\nSHA1sum: "+convertS(MessageDigest.getInstance("sha1").digest(bytes))
                                        +"\nSHA256sum: "+convertS(MessageDigest.getInstance("sha256").digest(bytes));

                    }catch (NoSuchAlgorithmException e){

                    }
                    runOnUiThread(changeText);

                    try {
                        PackageManager pm = getApplicationContext().getPackageManager();
                        PackageInfo mPackageInfo;
                        if (inIntent != null && inIntent.getAction() != null) {

                            if (inIntent.getAction().equals(Intent.ACTION_VIEW)) {
                                mPackageInfo = pm.getPackageArchiveInfo(UriUtils.pathUriCache(getApplicationContext(),uriFromIntent,"cache.apk"),//PackageManager.GET_SIGNATURES);
                                        Build.VERSION.SDK_INT >= 29 ? PackageManager.GET_SIGNATURES | PackageManager.GET_SERVICES : PackageManager.GET_SIGNATURES);
                                if (Build.VERSION.SDK_INT >= 29) sha256 +=  apkIsolatedZygote(mPackageInfo, "\n\n" + getString(R.string.app_zygote));
                                sha256 += apkCert(mPackageInfo);
                            }
                        }else {
                            if (inIntent.getStringExtra(MainActivity.CLICK_PRESS) != null && inIntent.getStringExtra(MainActivity.CLICK_PRESS).equals("shortClick"))
                                mPackageInfo = pm.getPackageInfo(inIntent.getStringExtra(MainActivity.EXTRA_PACKAGE_NAME),//PackageManager.GET_SIGNATURES);
                                        Build.VERSION.SDK_INT >= 29 ? PackageManager.GET_SIGNATURES | PackageManager.GET_SERVICES : PackageManager.GET_SIGNATURES);
                            else mPackageInfo = pm.getPackageArchiveInfo(uriFromIntent.getPath(),//PackageManager.GET_SIGNATURES);
                                    Build.VERSION.SDK_INT >= 29 ? PackageManager.GET_SIGNATURES | PackageManager.GET_SERVICES : PackageManager.GET_SIGNATURES);
                            if (Build.VERSION.SDK_INT >= 29) sha256 += apkIsolatedZygote(mPackageInfo, "\n\n" + getString(R.string.app_zygote));
                            sha256 += apkCert(mPackageInfo);
                        }
                        runOnUiThread(changeText);

                    }catch (Exception e){
                        sha256 += uriFromIntent.getPath()
                                +"\n"+"FAILED to retrieve PackageInfo"+e.getMessage();
                    }
                }
            }).start();


            //sha256+= getProperXml(AXMLPrinter.getManifestXMLFromAPK(uriFromIntent.getPath()));

            mProgressDialog.setMessage(sha256);
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();

            new FillClassesNamesThread(bytes).start();

            new StartDexLoaderThread(bytes).start();

        } catch (Exception e) {
            e.printStackTrace();
            ActivityCompat.finishAffinity(this);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        String statsMsg = "";
        int i, j = 0;
        switch (id) {
            case R.id.action_subs:
                subStats();
                return true;
            case R.id.action_settings:
                statsMsg = Names[0] + "\n";
                j = 1;
                for (i = 1; i < Names.length; i++) {
                    if (Names[i - 1].equals(Names[i])) continue;
                        statsMsg += Names[i] + "\n"; j++;
                }
                new AlertDialog.Builder(this).setTitle(j + " trackers (<=> "+Names.length+" signatures)")
                        .setNegativeButton(android.R.string.ok, null)
                        .setMessage(statsMsg).show();
                return true;
            case R.id.action_toggle:
                if(longClick) {
                    adapter = new StableArrayAdapter(ClassesListActivity.this,
                            android.R.layout.simple_list_item_1, classesList.getClassNames());
                    //subStats();
                    bar.setIcon(R.mipmap.ic_launcher_round);
                }else {
                    adapter = new StableArrayAdapter(ClassesListActivity.this,
                            android.R.layout.simple_list_item_1, classesListAll.getClassNames());
                    bar.setIcon(R.mipmap.ic_launcher);
                }
                longClick = !longClick;
                lv.setAdapter(adapter);
                return true;
            case R.id.action_super:
                Intent viewManifestIntent = new Intent(this, ViewManifestActivity.class);
                if(inIntent.getStringExtra(MainActivity.EXTRA_PACKAGE_NAME) != null) {
                    viewManifestIntent.putExtra(EXTRA_PACKAGE_NAME,inIntent.getStringExtra(MainActivity.EXTRA_PACKAGE_NAME));
                }

                viewManifestIntent.setData(inIntent.getData());
                this.startActivity(viewManifestIntent);
                return true;
            case R.id.action_exit:
                ActivityCompat.finishAffinity(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void subStats() {
        String statsMsg = "";
        int i = 0;
        TextView showText = new TextView(this);

        if (Signz >= 0) {
            for (i = 0; i < Sign.length; i++) {
                if (SignB[i]) {
                    if (!statsMsg.contains(Names[i])) statsMsg += "\n*" + Names[i] + "\n";
                    statsMsg += Integer.toString(SignStat[i]) + Sign[i] + "\n";
                }
            }
        }
        showText.setText((i + " tested signatures on " + ClassTotalz + " classes\n("+totalTT+")\n\n"
                + msg + statsMsg
                + "\n\n" + sha256));
        showText.setMovementMethod(new ScrollingMovementMethod());
        showText.setTextIsSelectable(true);
        new AlertDialog.Builder(this)
                .setTitle(Totalz + " trackers = " + classesList.size() + " Classes")
                .setView(showText)
                .setIcon(R.mipmap.ic_launcher)
                .setNegativeButton(android.R.string.ok, null)
                .show();
    }

    private class FillClassesNamesThread extends Thread {
        private final byte[] bytes;

        public FillClassesNamesThread(byte[] bytes) {
            this.bytes = bytes;
        }

        @Override
        public void run() {

            try {
                Thread.currentThread().setPriority(Thread.MAX_PRIORITY);

                File incomeFile = File.createTempFile("classes" + Thread.currentThread().getId(), ".dex", getCacheDir());

                IOUtils.bytesToFile(bytes, incomeFile);

                File optimizedFile = File.createTempFile("opt" + Thread.currentThread().getId(), ".dex", getCacheDir());

                DexFile dx = DexFile.loadDex(incomeFile.getPath(),
                        optimizedFile.getPath(), 0);

                msg ="";
                Sign = getResources().getStringArray(R.array.trackers);
                SignStat = new int[Sign.length];
                SignB = new boolean[Sign.length];//Arrays.fill(SignB,false);
                for (Enumeration<String> classNames = dx.entries(); classNames.hasMoreElements(); ) {
                    String className = classNames.nextElement();
                    classesListAll.add(className);

                    ClassTotalz++;
                    if (className.length()>8) {
                        if (className.contains(".")){
                            for (Signz = 0; Signz < Sign.length; Signz++) {
                                totalTT++;//TESTINGonly
                                if (className.contains(Sign[Signz])) {
                                    classesList.add(className);
                                    SignStat[Signz]++;
                                    SignB[Signz] = true;
                                    if (msg.contains(Names[Signz])) break;
                                    else msg += Names[Signz] + "\n";
                                    Totalz++;
                                    break;
                                }
                            }
                        }
                    }
                }

                ClassesListActivity.this.deleteFile("*");
                incomeFile.delete();
                optimizedFile.delete();

            } catch (Exception e) {
                // ODEX, need to see how to handle
                e.printStackTrace();
            }


            ClassesListActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if(!longClick) adapter = new StableArrayAdapter(ClassesListActivity.this,
                            android.R.layout.simple_list_item_1, classesList.getClassNames());
                    else {
                        adapter = new StableArrayAdapter(ClassesListActivity.this,
                                android.R.layout.simple_list_item_1, classesListAll.getClassNames());
                    }
                    lv.setAdapter(adapter);

                    mProgressDialog.dismiss();
                    if(classesList.getClassNames().isEmpty() && ClassTotalz ==0) {
                        Toast.makeText(ClassesListActivity.this, inIntent.getStringExtra(MainActivity.CLICK_PRESS)+
                                "Sorry don't support /system ODEX", Toast.LENGTH_LONG).show();
                    }else {
                        if(longClick) {
                            Toast.makeText(ClassesListActivity.this, "LONG_click --> All Classes", Toast.LENGTH_LONG).show();
                        } subStats();
                    }
                }
            });
        }
    }

    private class StartDexLoaderThread extends Thread {
        private final byte[] bytes;

        public StartDexLoaderThread(byte[] bytes) {
            this.bytes = bytes;
        }

        @Override
        public void run() {
            try {
                Thread.currentThread().setPriority(Thread.MAX_PRIORITY);

                final DexClassLoader loader = DexLoaderBuilder.fromBytes(ClassesListActivity.this, bytes);

                ClassesListActivity.this.runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                            @Override
                            public void onItemClick(AdapterView<?> parent, View view,
                                                    int position, long id) {

                                Class<?> loadClass;
                                try {
                                    if(!longClick) loadClass = loader.loadClass(classesList.getClassName((int)(parent.getAdapter()).getItemId(position)));
                                    else loadClass = loader.loadClass(classesListAll.getClassName((int)(parent.getAdapter()).getItemId(position)));


                                    Reflector reflector = new Reflector(loadClass);

                                    Toast.makeText(ClassesListActivity.this, reflector.generateClassData()
                                            , Toast.LENGTH_LONG).show();
                                    String result = reflector.toString();

                                    Intent i = new Intent(ClassesListActivity.this,
                                            SourceViewerActivity.class);

                                    if(!longClick)i.putExtra(ClassesListActivity.SELECTED_CLASS_NAME,
                                            classesList.getClassName((int)(parent.getAdapter()).getItemId(position)));
                                    else i.putExtra(ClassesListActivity.SELECTED_CLASS_NAME,
                                            classesListAll.getClassName((int)(parent.getAdapter()).getItemId(position)));
                                    i.putExtra(ClassesListActivity.SELECTED_CLASS_DUMP,
                                            result);

                                    startActivity(i);

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Toast.makeText(ClassesListActivity.this, e.toString(), Toast.LENGTH_LONG).show();
                                }

                            }
                        });
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}

