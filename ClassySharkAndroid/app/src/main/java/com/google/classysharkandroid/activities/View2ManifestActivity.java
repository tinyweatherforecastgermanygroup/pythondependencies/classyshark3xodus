package com.google.classysharkandroid.activities;

import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;

import android.util.Base64;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.classysharkandroid.R;
import com.google.classysharkandroid.utils.PackageUtils;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.lang.ref.WeakReference;

import static com.google.classysharkandroid.utils.PackageUtils.apkPro;
import static com.oF2pks.applicationsinfo.utils.Utils.getProperXml;
import static com.oF2pks.applicationsinfo.utils.dynManifest.getXMLText;

/**
 * Activity that shows AndroidManifest.xml of any apps. Application package name must be passed as extra.
 * To correctly display returned xml file, we show it in a {@link WebView}. The way we do that
 * is not very natural, but it's the simplest way to do that.
 * So, asynchronously, we get raw xml string and save it to a file. Then we ask to WebView to display this file,
 * by this way, WebView auto detect xml and display it nicely.
 * File do not need to be kept. We delete it to keep used memory as low as possible, but anyway, each time
 * we will show application's manifest, the same file will be used, so used memory will not grow.
 */
public class View2ManifestActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    public static final String EXTRA_PACKAGE_NAME = "package_name";
    private ActionBar bar;

    private WebView mWebView;
    private ProgressDialog mProgressDialog;

    private static String code;
    private  PackageManager mPackageManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bar = getSupportActionBar();
        //bar.setDisplayHomeAsUpEnabled(true);
        bar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        bar.setCustomView(R.layout.manifest_bar);
        TextView tTitle = findViewById(R.id.title_text);
        assert tTitle != null;

        androidx.appcompat.widget.SearchView searchView = findViewById(R.id.search_butn);
        //searchView.setQueryHint("zz");
        searchView.setOnQueryTextListener(this);


        Button eScan = findViewById(R.id.scan_button);
        assert eScan != null;
        eScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PackageUtils.manifestPackage(getBaseContext().getResources(), code, View2ManifestActivity.this);
            }
        });

        mWebView = new WebView(this);
        WebSettings settings = mWebView.getSettings();
        settings.setBuiltInZoomControls(true);
        settings.setUseWideViewPort(true);
        setContentView(mWebView);

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(getString(R.string.loading));

        mPackageManager= getPackageManager();
        String packageName = getIntent().getStringExtra(EXTRA_PACKAGE_NAME);

        String applicationLabel = null;
        try {
            applicationLabel = getPackageManager().getApplicationInfo(packageName, 0).loadLabel(getPackageManager()).toString();
            new AlertDialog.Builder(this).setTitle("\u2139\u2668: " + applicationLabel)
                    .setNegativeButton(android.R.string.ok, null)
                    .setMessage(apkPro(mPackageManager.getPackageInfo(packageName,
                            PackageManager.GET_SIGNATURES
                                    | PackageManager.GET_PERMISSIONS)
                            , mPackageManager))
                    .show();
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            Toast.makeText(this, packageName + ": " + getString(com.oF2pks.applicationsinfo.R.string.app_not_installed), Toast.LENGTH_LONG).show();
            finish();
        }

        tTitle.setText("\u2622" + applicationLabel);
        new AsyncManifestLoader(View2ManifestActivity.this).execute(packageName);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void displayContent() {
        mWebView.setWebChromeClient(new MyWebChromeClient());
        mWebView.findAllAsync("ComponentFactory");
        mWebView.loadData(Base64.encodeToString(code.getBytes(), Base64.NO_PADDING),"text/xml","base64");
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        mWebView.findNext(true);
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        mWebView.findAllAsync(newText);
        return true;
    }

    private void handleError() {
        Toast.makeText(this, "R.string.error", Toast.LENGTH_LONG).show();
        finish();
    }

    final class MyWebChromeClient extends WebChromeClient {
        @Override
        public void onProgressChanged(WebView view, int progress) {
            if (progress == 100)
                showProgressBar(false);
        }
    }

    private void showProgressBar(boolean show) {
        if (show)
            mProgressDialog.show();
        else
            mProgressDialog.dismiss();
    }

    /**
     * This AsyncTask takes manifest file path as argument
     */
    private static class AsyncManifestLoader extends AsyncTask<String, Integer, Boolean> {
        private WeakReference<View2ManifestActivity> mActivity = null;

        private AsyncManifestLoader(View2ManifestActivity pActivity) {
            link(pActivity);
        }

        private void link(View2ManifestActivity pActivity) {
            mActivity = new WeakReference<View2ManifestActivity>(pActivity);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (mActivity.get() != null) mActivity.get().showProgressBar(true);
        }

        @Override
        protected Boolean doInBackground(String... strings) {
            String packageName = strings[0];
            XmlResourceParser xml = null;
            AssetManager mCurAm = null;
            Resources mCurResources = null;
            try {
                //https://stackoverflow.com/questions/35474016/store-and-extract-map-from-android-resource-file
                mCurAm = mActivity.get().createPackageContext(packageName,
                        CONTEXT_IGNORE_SECURITY | CONTEXT_INCLUDE_CODE).getAssets();
                mCurResources = new Resources(mCurAm, mActivity.get().getResources().getDisplayMetrics(), null);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
                return false;
            }
            try {
                xml = mCurAm.openXmlResourceParser("AndroidManifest.xml");
                //this.mInput.setText("/sdcard/" + getPkgName() + ".txt");
                code = getProperXml(getXMLText(xml, mCurResources).toString());
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }

            return (code != null);
        }

        /**
         * Do not hide progressDialog here, WebView will hide it when content will be displayed
         */
        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if (mActivity.get() != null) {
                if (result)
                    mActivity.get().displayContent();
                else
                    mActivity.get().handleError();
            }
        }
    }
}
