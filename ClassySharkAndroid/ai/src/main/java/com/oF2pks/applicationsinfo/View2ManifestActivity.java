package com.oF2pks.applicationsinfo;

import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;

import android.util.Base64;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Toast;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.URLEncoder;

import static com.oF2pks.applicationsinfo.utils.dynManifest.getXMLText;

public class View2ManifestActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    public static final String EXTRA_PACKAGE_NAME = "package_name";

    private WebView mWebView;
    private ProgressDialog mProgressDialog;

    private static String code;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);

        androidx.appcompat.widget.SearchView searchView = new SearchView(actionBar.getThemedContext());
        searchView.setOnQueryTextListener(this);

        ActionBar.LayoutParams layoutParams = new androidx.appcompat.app.ActionBar.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        actionBar.setCustomView(searchView, layoutParams);
        mWebView = new WebView(this);
        WebSettings settings = mWebView.getSettings();
        settings.setBuiltInZoomControls(true);
        settings.setUseWideViewPort(true);
        setContentView(mWebView);

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(getString(R.string.loading));

        String packageName = getIntent().getStringExtra(EXTRA_PACKAGE_NAME);

        String applicationLabel = null;
        try {
            applicationLabel = getPackageManager().getApplicationInfo(packageName, 0).loadLabel(getPackageManager()).toString();
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            Toast.makeText(this, R.string.app_not_installed, Toast.LENGTH_LONG).show();
            finish();
        }

        actionBar.setTitle("\u2622" + applicationLabel);
        new AsyncManifestLoader(View2ManifestActivity.this).execute(packageName);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void displayContent() {
        mWebView.setWebChromeClient(new MyWebChromeClient());
        mWebView.findAllAsync("ComponentFactory");
        if (Build.VERSION.SDK_INT >18) mWebView.loadData(Base64.encodeToString(code.getBytes(), Base64.NO_PADDING),"text/xml","base64");
        else mWebView.loadData(URLEncoder.encode(code).replaceAll("\\+"," "),"text/plain","UTF-8");
    }

    private void handleError() {
        Toast.makeText(this, R.string.error, Toast.LENGTH_LONG).show();
        finish();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        mWebView.findNext(true);
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        mWebView.findAllAsync(newText);
        return true;
    }

    final class MyWebChromeClient extends WebChromeClient {
        @Override
        public void onProgressChanged(WebView view, int progress) {
            if (progress == 100)
                showProgressBar(false);
        }
    }

    private void showProgressBar(boolean show) {
        if (show)
            mProgressDialog.show();
        else
            mProgressDialog.dismiss();
    }

    /**
     * This AsyncTask takes manifest file path as argument
     */
    private static class AsyncManifestLoader extends AsyncTask<String, Integer, Boolean> {
        private WeakReference<View2ManifestActivity> mActivity = null;

        private AsyncManifestLoader(View2ManifestActivity pActivity) {
            link(pActivity);
        }

        private void link(View2ManifestActivity pActivity) {
            mActivity = new WeakReference<View2ManifestActivity>(pActivity);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (mActivity.get() != null) mActivity.get().showProgressBar(true);
        }

        @Override
        protected Boolean doInBackground(String... strings) {
            String packageName = strings[0];
            XmlResourceParser xml = null;
            AssetManager mCurAm = null;
            Resources mCurResources = null;
            try {
                //Log.e("package",packageName);//https://stackoverflow.com/questions/35474016/store-and-extract-map-from-android-resource-file
                mCurAm = mActivity.get().createPackageContext(packageName,
                        CONTEXT_IGNORE_SECURITY | CONTEXT_INCLUDE_CODE).getAssets();
                mCurResources = new Resources(mCurAm, mActivity.get().getResources().getDisplayMetrics(), null);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
                return false;
            }
            try {
                xml = mCurAm.openXmlResourceParser("AndroidManifest.xml");
                //this.mInput.setText("/sdcard/" + getPkgName() + ".txt");
                code = com.oF2pks.applicationsinfo.utils.Utils.getProperXml(getXMLText(xml, mCurResources).toString());
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }

            return (code != null);
        }

        /**
         * Do not hide progressDialog here, WebView will hide it when content will be displayed
         */
        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if (mActivity.get() != null) {
                if (result)
                    mActivity.get().displayContent();
                else
                    mActivity.get().handleError();
            }
        }
    }
}
