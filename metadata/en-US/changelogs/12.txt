(USE https://f-droid.org/en/packages/com.oF2pks.applicationsinfo/ to scan ALL installed packages, like pre-installed keyboard)

# ExodusPrivacy NEW database (eof294),

372 exoTrackers(317) = 334+38 signatures for 283+34 identified trackers

**2019.Dec** all F-Droid apps total Exodus scan: 
https://gitlab.com/oF2pks/3xodusprivacy-toolbox/blob/master/summaryLIGHTmapboxNOacra262.txt 

More info : 
- https://reports.exodus-privacy.eu.org/api/trackers (381 signatures for 317 effective entities)
- https://gitlab.com/oF2pks/3xodusprivacy-toolbox (40 known additions ²/ETIP-standby °/missing µ/tools)
- https://etip.exodus-privacy.eu.org

