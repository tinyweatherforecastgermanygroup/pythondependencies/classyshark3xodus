/*
 * Copyright 2015 Google, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.classysharkandroid.activities;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.SearchView;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.classysharkandroid.R;
import com.google.classysharkandroid.adapters.CustomAdapter;
import com.oF2pks.applicationsinfo.utils.Utils;
import com.oF2pks.chairlock.LaunchActivity;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.TreeSet;

import static com.google.classysharkandroid.utils.IOUtils.clearApplicationData;
import static com.google.classysharkandroid.utils.IOUtils.deleteCache;
import static com.google.classysharkandroid.utils.PackageUtils.appPublicSourceDir;


public class MainActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    public static final String APP_NAME = "APP_NAME";
    public static final String EXTRA_PACKAGE_NAME = "package_name";
    public static final String CLICK_PRESS = "CLICK_PRESS";
    private ListView lv;
    private ArrayList<AppListNode> apps;
    private CustomAdapter adapter;

    private static Collator sCollator = Collator.getInstance();
    private static final int[] sSortMenuItemIdsMap = {com.oF2pks.applicationsinfo.R.id.action_sort_domain,
            com.oF2pks.applicationsinfo.R.id.action_sort_name, com.oF2pks.applicationsinfo.R.id.action_sort_pkg,
            com.oF2pks.applicationsinfo.R.id.action_sort_installation, com.oF2pks.applicationsinfo.R.id.action_sort_sharedid,
            };

    private static final int SORT_DOMAIN = 0;
    private static final int SORT_NAME = 1;
    private static final int SORT_PKG = 2;
    private static final int SORT_INSTALLATION = 3;
    private static final int SORT_SHAREDID =4;
    public static final String INSTANCE_STATE_SORT_BY = "sort_by";
    private int mSortBy;
    private String mSelinux = "Unknow";
    private ActionBar actionBar;


    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        deleteCache(this);
        clearApplicationData(this);
        super.onDestroy();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        deleteCache(this);
        clearApplicationData(this);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lv = (ListView) findViewById(R.id.listView);
        lv.setFastScrollEnabled(true);
        actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setTitle("\u2302");
        SearchView searchView = new SearchView(actionBar.getThemedContext());
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                lv.setAdapter(adapter);
                return true;
            }
        });

        ActionBar.LayoutParams layoutParams = new ActionBar.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        actionBar.setCustomView(searchView, layoutParams);
        actionBar.setHomeButtonEnabled(true);

        if (savedInstanceState != null) {
            mSortBy = savedInstanceState.getInt(INSTANCE_STATE_SORT_BY);
        }else mSortBy=SORT_NAME;

        init();
    }

    private void init() {
        mSelinux = getZinfo("getenforce", "", false);
        if (mSelinux.equals("Permissive"))
            actionBar.setSubtitle(Html.fromHtml("<font color='Red'>" + mSelinux + "</font>"));
        else
            actionBar.setSubtitle(mSelinux);
        String tmpZygote = "";
        int scrollPos=lv.getFirstVisiblePosition();
        apps = new ArrayList<>();
        final Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
        mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);

        final TreeSet<ResolveInfo> pkgAppsList = new TreeSet<ResolveInfo>(new Comparator() {
            public int compare(Object o1, Object o2) {
                ResolveInfo r1 = (ResolveInfo) o1;
                ResolveInfo r2 = (ResolveInfo) o2;
                String s1 = r1.activityInfo.applicationInfo.loadLabel(getPackageManager()).toString().toLowerCase()
                        + r1.activityInfo.packageName;
                String s2 = r2.activityInfo.applicationInfo.loadLabel(getPackageManager()).toString().toLowerCase()
                        + r2.activityInfo.packageName;
                return s1.compareTo(s2);
            }
        });
        pkgAppsList.addAll(getPackageManager().queryIntentActivities(mainIntent, 0));

        //final List pkgAppsList = getPackageManager().queryIntentActivities(mainIntent, 0);// no unicity

        PackageInfo ai;
        for (ResolveInfo info : pkgAppsList) {
            File file = new File(info.activityInfo.applicationInfo.publicSourceDir);
            String zz = " ";

            AppListNode aln = new AppListNode();
            aln.name = info.activityInfo.applicationInfo.packageName;
            aln.icon = info.activityInfo.applicationInfo.loadIcon(getPackageManager());
            aln.rlog = (getPackageManager().checkPermission(Manifest.permission.READ_LOGS,aln.name)== PackageManager.PERMISSION_GRANTED);
            aln.isPrivApp = ((info.activityInfo.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) != 0);
            aln.sName = (String) info.activityInfo.applicationInfo.loadLabel(getPackageManager());
            try {
                ai = (getPackageManager().getPackageInfo(aln.name,0));
                aln.dInstall = ai.firstInstallTime;
                aln.dUpadte = ai.lastUpdateTime;
                aln.shared = ai.sharedUserId;
                if (Build.VERSION.SDK_INT >= 29){
                    ServiceInfo[] si = (getPackageManager().getPackageInfo(aln.name,PackageManager.GET_SERVICES)).services;
                    if (si != null) {
                        boolean bZygote = true;
                        boolean bIsolated = true;
                        for (ServiceInfo s : si) {
                            if (bZygote && (s.flags & ServiceInfo.FLAG_USE_APP_ZYGOTE) != 0) {
                                tmpZygote += ">" + aln.name + "\n" + s.name + "\n\n";
                                zz += "\u26BF";
                                bZygote = false;
                            }
                            if (bIsolated && (s.flags & ServiceInfo.FLAG_ISOLATED_PROCESS) != 0) {
                                zz += "\u26A0";
                                bIsolated = false;
                            }
                            if (!bIsolated && !bZygote) break;
                        }
                    }
                }
            }catch (PackageManager.NameNotFoundException e) {
                aln.shared="°";
                aln.dUpadte = 0L;
                aln.dInstall = 0L;
            }
            aln.sName += zz;

            apps.add(aln);
        }
        if (tmpZygote.length() > 0 )
            Toast.makeText(this, getString(R.string.app_zygote) + "\n" + tmpZygote, Toast.LENGTH_LONG).show();

        adapter=new CustomAdapter(this, apps, true);
        lv.setAdapter(adapter);
        lv.setSelection(scrollPos);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(INSTANCE_STATE_SORT_BY, mSortBy);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onStart() {
        super.onStart();

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                AppListNode aln = (AppListNode)parent.getAdapter().getItem(position);
                File file = appPublicSourceDir(getBaseContext(), aln.name);
                if (file !=null) {
                    MimeTypeMap myMime = MimeTypeMap.getSingleton();
                    Intent newIntent = new Intent(MainActivity.this, ClassesListActivity.class);
                    String mimeType = myMime.getMimeTypeFromExtension("apk");
                    newIntent.setDataAndType(Uri.fromFile(file),mimeType);
                    newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    newIntent.putExtra(APP_NAME, aln.sName);
                    newIntent.putExtra(EXTRA_PACKAGE_NAME, aln.name);
                    newIntent.putExtra(CLICK_PRESS, "shortClick");
                    try {
                        startActivity(newIntent);
                    } catch (ActivityNotFoundException e) {

                    }
                } else {
                    Toast.makeText(getBaseContext(), aln.sName + ": " + getString(com.oF2pks.applicationsinfo.R.string.app_not_installed), Toast.LENGTH_LONG).show();
                }
            }
        });

        lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view,
                                           int position, long id) {
                AppListNode aln = (AppListNode)parent.getAdapter().getItem(position);
                if (aln.name.equals("android")
                        || aln.name.equals(getBaseContext().getPackageName())) {
                    Intent newIntent = new Intent(MainActivity.this, ViewManifestActivity.class);
                    newIntent.putExtra(ViewManifestActivity.EXTRA_PACKAGE_NAME, aln.name);
                    newIntent.putExtra(CLICK_PRESS, "longClick");
                    try {
                        startActivity(newIntent);
                    } catch (ActivityNotFoundException e) {
                    }
                }else {
                    if (appPublicSourceDir(getBaseContext(), aln.name) != null) {
                        Intent newIntent = new Intent(MainActivity.this, View2ManifestActivity.class);
                        newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        newIntent.putExtra(EXTRA_PACKAGE_NAME, aln.name);
                        newIntent.putExtra(CLICK_PRESS, "longClick");
                        try {
                            startActivity(newIntent);
                        } catch (ActivityNotFoundException e) {
                        }
                    } else {
                        Toast.makeText(getBaseContext(), aln.sName + ": " + getString(com.oF2pks.applicationsinfo.R.string.app_not_installed), Toast.LENGTH_LONG).show();
                    }
                }
                return true;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_launch_sort, menu);
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.findItem(sSortMenuItemIdsMap[mSortBy]).setChecked(true);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_subs) {
            new AlertDialog.Builder(this).setTitle("Exodus global/world stats,")
                    .setNegativeButton(android.R.string.ok, null)
                    .setMessage(R.string.exostats).show();
            return true;
        } else if (id == R.id.action_settings) {
            new AlertDialog.Builder(this).setTitle("LongPress -> dynamic/☢ Manifest,")
                    .setNegativeButton(android.R.string.ok, null)
                    .setMessage(R.string.exoweb).show();
            return true;
        } else if (id == R.id.action_toggle) {
            startActivity(new Intent(this, com.oF2pks.applicationsinfo.MainActivity.class));//.putExtra("perm_name", "Onboard.packages"));
            return true;
        } else if (id == R.id.action_exit) {
            ActivityCompat.finishAffinity(this);
            return true;
        } else if (id == R.id.action_super) {
            startActivity(new Intent(this, LaunchActivity.class));
            return true;
        } else if (id == com.oF2pks.applicationsinfo.R.id.action_refresh) {
            init();
            setSortBy(mSortBy);
            return true;
        } else if (id == com.oF2pks.applicationsinfo.R.id.action_sort_name) {
            adapter = new CustomAdapter(this, apps, true);
            lv.setAdapter(adapter);
            setSortBy(SORT_NAME);
            item.setChecked(true);
            return true;
        } else if (id == com.oF2pks.applicationsinfo.R.id.action_sort_pkg) {
            adapter = new CustomAdapter(this, apps, false);
            lv.setAdapter(adapter);
            setSortBy(SORT_PKG);
            item.setChecked(true);
            return true;
        } else if (id == com.oF2pks.applicationsinfo.R.id.action_sort_domain) {
            setSortBy(SORT_DOMAIN);
            item.setChecked(true);
            return true;
        } else if (id == com.oF2pks.applicationsinfo.R.id.action_sort_installation) {
            setSortBy(SORT_INSTALLATION);
            item.setChecked(true);
            return true;
        } else if (id == com.oF2pks.applicationsinfo.R.id.action_sort_sharedid) {
            setSortBy(SORT_SHAREDID);
            item.setChecked(true);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void setSortBy(int sort) {
        mSortBy = sort;
        Collections.sort(apps, new Comparator<AppListNode>() {
            @Override
            public int compare(AppListNode item1, AppListNode item2) {
                switch (mSortBy) {
                    case SORT_NAME:
                        return sCollator.compare(item1.sName, item2.sName);
                    case SORT_PKG:
                        return item1.name.compareTo(item2.name);
                    case SORT_DOMAIN:
                        return Utils.compareBooleans(item1.isPrivApp, item2.isPrivApp);
                    case SORT_INSTALLATION:
                        //Sort in decreasing order
                        return -item1.dUpadte.compareTo(item2.dUpadte);
                    case SORT_SHAREDID:
                        return -item1.dInstall.compareTo(item2.dInstall);
                    default:
                        return 0;
                }
            }
        });

        if (adapter != null)
            adapter.notifyDataSetChanged();
    }

    public class AppListNode {
        public String name;
        public Drawable icon;
        public boolean rlog;
        public String shared;
        public boolean isPrivApp;
        public String sName;
        public Long dInstall;
        public Long dUpadte;
    }
    static String getZinfo(String s, String sPlus, boolean bool) {
        try {
            Process p = Runtime.getRuntime().exec(s);
            InputStream is = null;
            //if (p.waitFor() == 0) {
            is = p.getInputStream();
            /*} else {
                is = p.getErrorStream();
            }*/
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String tmp;
            String tmp2 = "";

            if (bool) br.readLine();
            while ((tmp = br.readLine()) != null)
            {
                tmp2 +=sPlus+tmp;
            }
            is.close();
            br.close();
            p.destroy();
            if (tmp2.length() !=0) return tmp2;
            return "Unknow";
        } catch (Exception ex) {
            return "ERROR: " + ex.getMessage();
        }
    }

}
