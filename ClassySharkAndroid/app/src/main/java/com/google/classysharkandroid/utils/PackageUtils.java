package com.google.classysharkandroid.utils;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PermissionInfo;
import android.content.pm.ServiceInfo;
import android.content.pm.Signature;
import android.content.res.Resources;
import android.widget.TextView;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

import static com.oF2pks.applicationsinfo.utils.Utils.getProtectionLevelString;

import androidx.appcompat.app.AlertDialog;

import com.google.classysharkandroid.R;

public class PackageUtils {

    public static String apkIsolatedZygote(PackageInfo p, String intro) {
        String sResult="";
        ServiceInfo[] si = p.services;
        if (si != null) {
            for (ServiceInfo s : si) {
                if ((s.flags & ServiceInfo.FLAG_USE_APP_ZYGOTE) != 0) {
                    sResult = s.name + ":" + sResult;
                    sResult += "\u26BF";
                }
            }
        }

        return sResult.length() == 0 ? "": intro + sResult;
    }

    public static String apkCert(PackageInfo p){
        Signature[] z= p.signatures;
        String s="";
        X509Certificate c;
        try {
            for (Signature sg:z){
                c=(X509Certificate) CertificateFactory.getInstance("X.509")
                        .generateCertificate(new ByteArrayInputStream(sg.toByteArray()));
                s="\n\n"+c.getIssuerX500Principal().getName()+"\n\n"+c.getSigAlgName();
                try{
                    s+= "\n\nCERTIFICATE fingerprints: \nmd5: "+convertS(MessageDigest.getInstance("md5").digest(sg.toByteArray()));
                    s+= "\nsha1: "+convertS(MessageDigest.getInstance("sha1").digest(sg.toByteArray()));
                    s+= "\nsha256: "+convertS(MessageDigest.getInstance("sha256").digest(sg.toByteArray()));

                }catch (NoSuchAlgorithmException e){
                    return e.getMessage();
                }
            }

        }catch (CertificateException e) {
            return e.getMessage();
        }
        return s;
    }

    public static String apkPro(PackageInfo p, PackageManager mPackageManager){
        String[] aPermissionsUse;
        String s=apkCert(p);
        String tmp="";
        PermissionInfo pI;

        if (p.requestedPermissions != null){
            aPermissionsUse= new String[p.requestedPermissions.length];
            for (int i=0;i < p.requestedPermissions.length;i++){
                if (p.requestedPermissions[i].startsWith("android.permission"))
                    aPermissionsUse[i]=p.requestedPermissions[i].substring(18)+" ";
                else aPermissionsUse[i]=p.requestedPermissions[i]+" ";
                try {
                    pI=mPackageManager.getPermissionInfo(p.requestedPermissions[i],PackageManager.GET_META_DATA);
                    tmp=getProtectionLevelString(pI.protectionLevel);
                    if (tmp.contains("dangerous")) aPermissionsUse[i]= "*\u2638"+aPermissionsUse[i];
                    aPermissionsUse[i]+= tmp+"\n-->"+pI.group;


                } catch (PackageManager.NameNotFoundException e){

                }
                if ((p.requestedPermissionsFlags[i] & PackageInfo.REQUESTED_PERMISSION_GRANTED) != 0) aPermissionsUse[i]+=" ^\u2714";


            }
            try {
                Arrays.sort(aPermissionsUse);
            } catch (NullPointerException e){
            }
            s+="\n";
            for (int i=0;i < aPermissionsUse.length;i++) {
                s += "\n\n" + aPermissionsUse[i];
            }
        }
        if (p.permissions != null){
            s+="\n\n#######################\n### Declared Permissions ###";
            try {
                Collections.sort(Arrays.asList(p.permissions), new Comparator<PermissionInfo>() {
                    public int compare(PermissionInfo o1, PermissionInfo o2) {
                        return o1.name.compareToIgnoreCase(o2.name);
                    }
                });
            } catch (NullPointerException e){

            }
            for (int i=0;i < p.permissions.length;i++) {
                s += "\n\n\u25a0"+p.permissions[i].name
                        +"\n"+p.permissions[i].loadLabel(mPackageManager)
                        +"\n"+p.permissions[i].loadDescription(mPackageManager)
                        +"\n"+p.permissions[i].group;
            }


        }
        return s;
    }

    public static String convertS(byte[] digest){
        String s="";
        for (byte b:digest){
            s+= String.format("%02X", b).toLowerCase();
        }
        return s;
    }

    public static File appPublicSourceDir(Context context, String packageName) {
        try {
            return new File(context.getPackageManager().getApplicationInfo(packageName, 0).publicSourceDir);
        }
        catch (PackageManager.NameNotFoundException e) {
            return null;
        }
    }

    public static void manifestPackage(Resources r, String code, Context ctx) {
        String s = "";
        String[] Sign;
        int Signz = 0 ;
        int Totalz=0;
        String[] Names;

        Names = r.getStringArray(R.array.tname);
        Sign = r.getStringArray(R.array.trackers);
        for (Signz = 0; Signz < Sign.length; Signz++) {
            if (code.contains(Sign[Signz])) {
                s += "_" +Names[Signz] + ": " + Sign[Signz] + "\n\n";
                Totalz++;
            }
        }
        //Log.e("zzz",s);
        TextView showText = new TextView(ctx);
        showText.setText("\u2211 = "+ Totalz + " exoTracker(s)\n\n" + s);
        showText.setTextIsSelectable(true);
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setView(showText)
                .setTitle(r.getString(R.string.exodus) + " " + r.getString(R.string.escan))
                .setIcon(R.mipmap.ic_launcher_round)
                .setCancelable(true)
                .setNegativeButton(android.R.string.ok, null)
                .show();
    }
}
