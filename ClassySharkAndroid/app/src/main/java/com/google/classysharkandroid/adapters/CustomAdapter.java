package com.google.classysharkandroid.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.BackgroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.classysharkandroid.R;
import com.google.classysharkandroid.activities.MainActivity;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class CustomAdapter extends BaseAdapter implements Filterable {
    private ArrayList<MainActivity.AppListNode> mNodes;
    private ArrayList<MainActivity.AppListNode> mNodesListFiltered;
    private boolean bToggle;
    private static LayoutInflater inflater = null;
    private BackgroundColorSpan colorSpan= new BackgroundColorSpan(Color.parseColor("#FF9702"));
    static final DateFormat sSimpleDateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss ");


    public CustomAdapter(Activity a, ArrayList<MainActivity.AppListNode> appNode, boolean b){
        mNodes= appNode;
        mNodesListFiltered= appNode;
        bToggle =b;
        inflater = (LayoutInflater)a.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return mNodesListFiltered.size();
    }

    public MainActivity.AppListNode getItem(int position) {
        return mNodesListFiltered.get(position);
    }

    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        View row= convertView;
        SpannableString ss;

        if(convertView==null) row = inflater.inflate(R.layout.row, null);
        TextView title, line , update, install;
        MainActivity.AppListNode n = mNodesListFiltered.get(position);
        ImageView i1=row.findViewById(R.id.imgIcon);
        title = row.findViewById(R.id.txtTitle);
        line = row.findViewById(R.id.txtPackage);
        install = row.findViewById(R.id.txtDateInstall);
        update = row.findViewById(R.id.txtDateUpdate);


        //if (n.isPrivApp) title.setBackgroundColor(Color.parseColor("#ffe6e6e6"));else title.setBackgroundColor(Color.parseColor("#fff6f6f6"));
        if (n.shared==null) {
            ss = new SpannableString(n.sName);
            install.setTextColor(Color.GRAY);
        }
        else {
            ss = new SpannableString(n.sName + ":@" + n.shared);
            install.setTextColor(Color.parseColor("#FF9702"));
            ss.setSpan(new RelativeSizeSpan(.8f),n.sName.length(),n.sName.length()+n.shared.length()+2,Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        if (!bToggle) {
            title.setText(n.name);
            line.setText(ss);
        } else {
            title.setText(ss);
            line.setText(n.name);
        }
        title.setTextColor(n.isPrivApp ? Color.parseColor("#FF9702") : Color.WHITE);
        update.setTextColor(n.rlog ? Color.RED : Color.GRAY);
        i1.setImageDrawable(n.icon);
        update.setText((n.dUpadte.equals(n.dInstall) && !n.rlog) ? "" : ""+com.oF2pks.applicationsinfo.utils.Utils.simpleDate("" + new Date(n.dUpadte)));
        install.setText(
                "["+TimeUnit.DAYS.convert( (new Date()).getTime()-(new Date(n.dUpadte)).getTime() ,TimeUnit.MILLISECONDS)
                +"+"+TimeUnit.DAYS.convert(n.dUpadte-n.dInstall,TimeUnit.MILLISECONDS)
                +"]      "+sSimpleDateFormat.format(new Date(n.dInstall)));

        return (row);
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if(constraint == null || constraint.length() == 0){
                    filterResults.count = mNodes.size();
                    filterResults.values = mNodes;
                }else{
                    ArrayList<MainActivity.AppListNode> resultAln = new ArrayList<>();
                    String searchStr = constraint.toString().toLowerCase();

                    for(MainActivity.AppListNode item:mNodes){
                        if(item.name.toLowerCase().contains(searchStr)
                                || item.sName.toLowerCase().contains(searchStr)) {
                            resultAln.add(item);
                        }
                        filterResults.count = resultAln.size();
                        filterResults.values = resultAln;
                    }
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                mNodesListFiltered = (ArrayList<MainActivity.AppListNode>) results.values;
                notifyDataSetChanged();
            }
        };
    }
}
