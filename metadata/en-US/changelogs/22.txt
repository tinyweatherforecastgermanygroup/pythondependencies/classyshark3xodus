# ExodusPrivacy NEW database (eof328),

407 exoTrackers(345) = 374+33 signatures for 315+30 identified trackers

More info : 
- https://reports.exodus-privacy.eu.org/api/trackers
- https://gitlab.com/oF2pks/3xodusprivacy-toolbox (32 known additions ²/ETIP-standby °/missing µ/tools)
- https://etip.exodus-privacy.eu.org


*Exception to Exodus database: Mapbox is softened to MapboxTelemetry instead of the wider general map sdk detection https://docs.mapbox.com/android/maps/overview/#telemetry-opt-out
